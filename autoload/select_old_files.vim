" Entry point to command function with one optional argument, the count of
" v:oldfiles wanted for selection.
"
function! select_old_files#(...) abort

  " Check we didn't receive too many arguments
  if a:0 > 1
    echoerr 'Too many arguments'
  endif

  " If an argument was provided, use that as the limit; failing that, use
  " a global variable g:select_old_files_limit if set; failing that, use two
  " less than the current number of screen lines, filling the screen but not
  " forcing a pager.
  "
  let limit = a:0 == 1
        \ ? a:1 : get(g:, 'select_old_files_limit', &lines - 2)

  " Check the count provided makes sense: a non-zero positive integer
  if limit <= 0
    echoerr 'Invalid count'
  endif

  " Save the current value of v:oldfiles and reassign it to have the number of
  " items specified, and then :browse it.  Once that's done, whether or not
  " the user chose a file to edit, put the previous v:oldfiles value back.
  "
  let oldfiles = v:oldfiles
  let v:oldfiles = v:oldfiles[:limit - 1]
  browse oldfiles
  let v:oldfiles = oldfiles

endfunction
