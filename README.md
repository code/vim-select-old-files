select\_old\_files.vim
======================

Slightly enhanced `:browse oldfiles` that limits the count of selectable files,
defaulting to just short of the window height, to avoid a pager.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
