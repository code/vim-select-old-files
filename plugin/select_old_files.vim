"
" select_old_files.vim: Slightly enhanced `:browse oldfiles` that limits the
" count of selectable files, defaulting to just short of the window height, to
" avoid a pager.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_select_old_files') || &compatible || !exists(':oldfiles')
  finish
endif
let loaded_select_old_files = 1

" User command accepts single optional integer limiting files listing
command! -bar -nargs=? SelectOldFiles
      \ call select_old_files#(<f-args>)

" Normal mode mapping calls command with no arguments
nnoremap <Plug>(SelectOldFiles)
      \ :<C-U>SelectOldFiles<CR>
